import * as React from "react";

interface Props {
  thing: string;
}

interface GetThingPage {
  (props: Props): JSX.Element;
  getInitialProps(): Promise<Partial<Props>>;
}

const GetThing: GetThingPage = Object.assign(
  ({ thing }: Props) => {
    console.log(thing);
    return <div>{thing}</div>;
  },
  {
    getInitialProps() {
      return getThingAsync()
        .catch(err => err)
        .then(thing => ({ thing }));
    }
  }
);

export default GetThing;

function getThingAsync() {
  return new Promise((res, rej) => {
    setTimeout(() => {
      const result = Math.random();

      if (result > 0.5) {
        res(`the result was ${result}`);
      } else {
        rej("Tears 😭");
      }
    }, 1);
  });
}
