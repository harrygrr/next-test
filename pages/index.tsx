import * as React from "react";

import { Dispatch } from "react-redux";
import { State, Action, AddToCart, makeStore } from "../store";
import withRedux from "next-redux-wrapper";

interface DispatchMappedToProps {
  addItem: (id: number) => any;
}

interface Props extends State, DispatchMappedToProps {}

function Index(props: Props) {
  return (
    <div>
      Heya!, whatsup?
      <pre>{JSON.stringify(props, null, 2)}</pre>
      <button onClick={() => props.addItem(1)}>Add Item</button>
    </div>
  );
}

export default withRedux<State, State, DispatchMappedToProps>(
  makeStore,
  (state: State) => state,
  (dispatch: Dispatch<Action>) => ({
    addItem: (id: number) => dispatch(AddToCart(id))
  })
)(Index);
