import { createStore, applyMiddleware } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
import thunkMiddleware from "redux-thunk";
import { adjust, merge } from "ramda";

interface Product {
  id: number;
  title: string;
  price: number;
}

interface CartItem {
  product: number;
  quantity: number;
}

export interface State {
  products: Product[];
  cart: CartItem[];
}
const initialState: State = {
  products: [
    { id: 1, title: "first product", price: 1020 },
    { id: 2, title: "second product", price: 1340 }
  ],
  cart: []
};

export interface SetProducts {
  type: "SetProducts";
  products: Product[];
}
export function SetProducts(products: Product[]): SetProducts {
  return { type: "SetProducts", products };
}

export interface AddToCart {
  type: "AddToCart";
  id: number;
}

export function AddToCart(id: number): AddToCart {
  return { type: "AddToCart", id };
}

export type Action = AddToCart;

// REDUCERS
export const reducer = (state: State = initialState, action: Action) => {
  switch (action.type) {
    case "AddToCart":
      const cartItem = state.cart.findIndex(item => item.product === action.id);
      return {
        ...state,
        cart:
          cartItem === -1
            ? state.cart.concat({ product: action.id, quantity: 1 })
            : adjust(
                item => merge(item, { quantity: item.quantity + 1 }),
                cartItem,
                state.cart
              )
      };

    default:
      return state;
  }
};

export const makeStore = (state: State = initialState) => {
  return createStore(
    reducer,
    state,
    composeWithDevTools(applyMiddleware(thunkMiddleware))
  );
};

export default makeStore();
